<!DOCTYPE html>
<html>
   <head>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css" />
   </head>
   <body>
        <table class="table table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
<th><tr><td>Data</td><td>Date</td></tr></th>
<tb>
<?php
        $f = fopen("data.csv", "r+");
        $i = 0;
        while ((($a = fgetcsv($f, 1000, ";")) !== FALSE) and ($i <= 100)) {
                $i = $i + 1;
	        echo "<tr><td>".$a[1]."</td><td>".date('l jS \of F Y h:i:s A', intval($a[0]))."</td></tr>\n";
        }
?>
</tb>
</table>
</body>
</html>
