<!DOCTYPE HTML>
<html lang="en">
<head>
<title>Солнечный трекер</title>
<meta http-equiv="Refresh" content="120"/>
<meta charset="UTF-8">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body>
<?php
include "style.html";
include "header_and_nav.html";
?>
<article>
<div class="button is-large is-fullwidth
<?php
        $f = fopen("commands.csv", "r+");
        $b = fgetcsv($f, 1000, ";");
        if ($b[0] != "n") echo "is-loading";
        fclose($f);
?>
">
Команды выполнены
</div>

<table class="table">
    <tb>
        <tr>
          <td><a class="button is-success is-fullwidth" href="on.php">Вкл</a></td>
          <td><a class="button is-warning is-fullwidth" href="up.php">▲</a>
          <td><a class="button is-danger is-fullwidth" href="off.php">Выкл</a></td>
        </tr>
        
        <tr>
          <td><a class="button is-warning is-fullwidth" href="left.php">◄</a></td>
          <td><a class="button is-warning is-fullwidth" href="down.php">▼</a></td>
          <td><a class="button is-warning is-fullwidth" href="right.php">►</a></td>
        </tr>
    </tb>
</table>

<div class = "charter">
<div id = "chart">
</div>
</div>
</article>
<?php
include "footer.html";
?>
</body>
<script type="text/javascript">
var h = document.documentElement.clientHeight * 0.4;
var el = document.getElementById("chart");
el.style.height = h;
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function standartDate(t){
    d = new Date(t * 1000);
    return d;
}
function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Время', 'Ток к/з на солнечной батарее (А)'],
        <?php
        $f = fopen("data.csv", "r");
        while($pair = fgetcsv($f, 1000, ";")) {
            if (is_numeric($pair[1])) echo "[standartDate(".$pair[0]."),".$pair[1]."],";
        }
        fclose($f);
        ?>
    ]);
    var options = {
        title: 'Освещенность',
        legend: { position: 'bottom' }
    };
    var chart = new google.visualization.LineChart(document.getElementById('chart'));
    chart.draw(data, options);
    }
function f(){
document.body.style.margin = String(document.documentElement.clientWidth * 0.05) +  "px";
document.body.style.width = String(document.documentElement.clientWidth * 0.90) +  "px";
}
f();
setInterval(f, 1000);
</script>

</html>
